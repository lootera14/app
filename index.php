<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>SWFTEA</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/swftea.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Appland - v2.2.0
  * Template URL: https://bootstrapmade.com/free-bootstrap-app-landing-page-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top  header-transparent ">
    <div class="container d-flex align-items-center">

      <div class="logo mr-auto">
        <h1 class="text-light"><a href="index.php" style="font-size: 34px;">SWFTEA</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="index.php">Home</a></li>
          <li><a href="#features">App Features</a></li>
          <li><a href="#details">About Us</a></li>
          <li><a href="#gallery">Gallery</a></li>
          <li><a href="#pricing">Pricing</a></li>
          <!-- <li><a href="#faq">F.A.Q</a></li> -->
          <li><a href="#contact">Contact Us</a></li>

          <li class="get-started"><a href="#features">Get Started</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-lg-flex flex-lg-column justify-content-center align-items-stretch pt-5 pt-lg-0 order-2 order-lg-1" data-aos="fade-up">
          <div>
            <h1>Sharing Worldwide Fun Treasure Entertainment App(SWFTEA)</h1>
            <h2></h2>
            <a href="https://play.google.com/store/apps/details?id=com.swftea.project&hl=en" class="download-btn"><i class="bx bxl-play-store"></i> Google Play</a>
            <!-- <a href="#" class="download-btn"><i class="bx bxl-apple"></i> App Store</a> -->
          </div>
        </div>
        <div class="col-lg-6 d-lg-flex flex-lg-column align-items-stretch order-1 order-lg-2 hero-img" data-aos="fade-up">
          <img src="assets/img/hero-img.png" class="img-fluid" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= App Features Section ======= -->
    <section id="features" class="features">
      <div class="container">

        <div class="section-title">
          <h2>App Features</h2>
          <p>SWFTEA(Sharing Worldwide Fun Treasure Entertainment App) in general is a virtual entertainment chatting platform where anyone can access the account from any part of the world.</p>
        </div>

        <div class="row no-gutters">
          <div class="col-xl-7 d-flex align-items-stretch order-2 order-lg-1">
            <div class="content d-flex flex-column justify-content-center">
              <div class="row">
                <div class="col-md-6 icon-box" data-aos="fade-up">
                  <i class="bx bx-comment"></i>
                  <h4>Private Chat</h4>
                  <p>Users can have private chat option where they can chat, send gifts, images, voice messages and view user profile.</p>
                </div>
                <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="100">
                  <i class="bx bx-group"></i>
                  <h4>Group Chat</h4>
                  <p>Users can group selected friends and have a group chat which incorporates all the private chat features.</p>
                </div>
                <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="200">
                  <i class="bx bx-chat"></i>
                  <h4>Chat Rooms</h4>
                  <p>Chat Rooms are common places for all users to hang out and meet new users from around the globe. Users can interact with any people who join the same chatroom and make new friends with chat, gifts and games.</p>
                </div>
                <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="300">
                  <i class="bx bx-game"></i>
                  <h4>Play Games</h4>
                  <p>Users will have access to chatroom games like Lowcard, Cricket, Lucky7, Guess bot. Game Rooms allow user to play different games with different users from around the globe.</p>
                </div>
                <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="400">
                  <i class="bx bx-atom"></i>
                  <h4>Mentor/Merchant Programme</h4>
                  <p>Mentor/Merchant Programme is an affiliate marketing base programme where you can earn via credit sales. Different packages are available for purchase and retail sales, the more you sell the more you earn. Join today and start earning.</p>
                </div>
                <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="400">
                  <i class="bx bx-gift"></i>
                  <h4>Gifts</h4>
                  <p>Gift shower is a feature for sending virtual gifts. Different types of gifts are available on the store which can be sent via commands; privately or to a user, at group-chats or chat rooms</p>
                </div>
                <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="400">
                  <i class="bx bx-store"></i>
                  <h4>Custom Store</h4>
                  <p>The in-app store allows you to view and buy different gifts and emoticons which can be sent at private chats or chat rooms which makes your chat even more fun.</p>
                </div>
                <div class="col-md-6 icon-box" data-aos="fade-up" data-aos-delay="500">
                  <i class="bx bx-id-card"></i>
                  <h4>User Profile</h4>
                  <p>Every user has his/her own profile which shows all the achievents of the user. The profile feature allows you to add your profile image, shows user level, number of gifts sent and received, footprints of users who viewed your profile and number of friends.</p>
                </div>
              </div>
            </div>
          </div>
          <div class="image col-xl-5 d-flex align-items-stretch justify-content-center order-1 order-lg-2" data-aos="fade-left" data-aos-delay="100">
            <img src="assets/img/features.svg" class="img-fluid" alt="">
          </div>
        </div>

      </div>
    </section><!-- End App Features Section -->

    <!-- ======= Details Section ======= -->
    <section id="details" class="details">
      <div class="container">

        <div class="row content">
          <div class="col-md-4" data-aos="fade-right">
            <img src="assets/img/details-1.png" class="img-fluid" alt="">
          </div>
          <div class="col-md-8 pt-4" data-aos="fade-up">
            <h3>ABOUT US</h3>
            <p>
              SWFTEA APP - In general, is a virtual entertainment chatting platform where anyone can have virtual entertainment with the real feeling from any corner of the world. Chatting with various parts of the world by joining chatrooms, having group chats, hanging on private chats, etc are the common features, while  various In-app games, virtual gifts sharing and even audio voice sharing are best portion to influence the friendship
            </p>
            
            <p>
              SWFTEA stands for Sharing Worldwide Fun Treasure Entertainment Apps . Here, anyone can register an account using a verified email address with a unique username. A registered user can access many features inside the account like uploading profile pictures and cover pictures including personal details. A user has a right to create chatroom(s) and access entry in any of chatrooms until and unless they are banned for appropriate violence of terms and conditions or any misbehaves being reported. Adding friends to create a new friendship relations and having virtual dates are another interesting factor.
            </p>
          </div>
        </div>

        <div class="row content">
          <div class="col-md-4 order-1 order-md-2" data-aos="fade-left">
            <img src="assets/img/details-2.png" class="img-fluid" alt="">
          </div>
          <div class="col-md-8 pt-5 order-2 order-md-1" data-aos="fade-up">
            <h3>SWFTEA is our dream project to build a strong, loving and user-friendly community around the globe.</h3>
            <p>
              SWFTEA will be continuously developing its features to establish a user loving app with the feeling of best user experience. We have a team of experienced developers, designers, Testers and marketers, who are always there for assisting SWFTEA to enhance its features to the best. Our supporting official(s), Admin(s), and even Legend(s) are always 27x7 online to receive user's feedback and suggest and or guide users with the existing feature(s) or with the exciting announcement(s).
            </p>
          </div>
        </div>

      </div>
    </section><!-- End Details Section -->

    <!-- ======= Gallery Section ======= -->
    <section id="gallery" class="gallery">
      <div class="container">

        <div class="section-title">
          <h2>Gallery</h2>
        </div>

        <div class="owl-carousel gallery-carousel" data-aos="fade-up">
          <a href="assets/img/gallery/account.png" class="venobox" data-gall="gallery-carousel"><img src="assets/img/gallery/account.png" alt=""></a>
          <a href="assets/img/gallery/menu.png" class="venobox" data-gall="gallery-carousel"><img src="assets/img/gallery/menu.png" alt=""></a>
          <a href="assets/img/gallery/store.png" class="venobox" data-gall="gallery-carousel"><img src="assets/img/gallery/store.png" alt=""></a>
          <a href="assets/img/gallery/chatroom.png" class="venobox" data-gall="gallery-carousel"><img src="assets/img/gallery/chatroom.png" alt=""></a>
          <a href="assets/img/gallery/profile.png" class="venobox" data-gall="gallery-carousel"><img src="assets/img/gallery/profile.png" alt=""></a>
          <a href="assets/img/gallery/people.png" class="venobox" data-gall="gallery-carousel"><img src="assets/img/gallery/people.png" alt=""></a>
          <a href="assets/img/gallery/announcement.png" class="venobox" data-gall="gallery-carousel"><img src="assets/img/gallery/announcement.png" alt=""></a>
          <a href="assets/img/gallery/explorer.png" class="venobox" data-gall="gallery-carousel"><img src="assets/img/gallery/explorer.png" alt=""></a>
          <a href="assets/img/gallery/leaderboard.png" class="venobox" data-gall="gallery-carousel"><img src="assets/img/gallery/leaderboard.png" alt=""></a>
          <a href="assets/img/gallery/notification.png" class="venobox" data-gall="gallery-carousel"><img src="assets/img/gallery/notification.png" alt=""></a>
          <a href="assets/img/gallery/search.png" class="venobox" data-gall="gallery-carousel"><img src="assets/img/gallery/search.png" alt=""></a>
          <a href="assets/img/gallery/transfer.png" class="venobox" data-gall="gallery-carousel"><img src="assets/img/gallery/transfer.png" alt=""></a>
        </div>

      </div>
    </section><!-- End Gallery Section -->

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials section-bg">
      <div class="container">

        <div class="section-title">
          <h2>Testimonials</h2>
        </div>

        <div class="owl-carousel testimonials-carousel" data-aos="fade-up">

          <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/annabelle.jpg" class="testimonial-img" alt="">
              <h3>Annabelle</h3>
              <h5>'Indonesia'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                dari awal join SWFTEA saya suka banget dengan app chat ini.. dari sisi staff nya mereka semua welcome & responnya bagus ke semua users tanpa ada perbedaan.. appnya juga bagus, senang sekali bisa kenal sama orang-orang baru, bisa punya teman dari negara luar.. di sini kita di satukan seperti keluarga, meski budaya berbeda tetapi bisa happy bareng.. & nilai plusnya saya jadi tau beberapa sedikit bahasa dr app ini.. (hehe..) pokoknya app nya joss dahhh... love bangettt sma app SWFTEA. xoxo... :D
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>

          <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/sthashristy.jpg" class="testimonial-img" alt="">
              <h3>Sthashristy</h3>
              <h5>'Nepal'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                I would like to  thank you to all entire owner & swftea team  for a such a wonderful app. At first  while entering in this app i used to play PUBG most of the time through which i was yelled by my parent, then my friend suggest me about the swftea, i signned up n i got lots of positive vibes & i used to spend most of the time at swftea through which i have got such a wonderful friends to whom i can share my felling,my happiness,my sandness. most of the user have lots of problem same like as me  first time i also used to have some problem bt they were so postive that they always rectify all the problem without any excuses & delay. I haven't seen such app before where i get so close to all the user. Thank you very much for making such a wonderful app n i wish to god more progress will be done in your coming days. God bless you all n lots of love.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>

          <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/nimisis.png" class="testimonial-img" alt="">
              <h3>Nimisis</h3>
              <h5>'UAE'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Dear SWFTEA team,
                I would like to thank you to all entire SWFTEA team for such a good app. Swftea has given me such a good friends as well as wonderful team management.
                Having as a mentor, it has changed my life for the better. I truly appreciate everything that you have done for us. If there is any way that I can repay you, please let me know. I look forward to working with you in the future together for forever. Honestly, as a mentor i have very good experience with SWFTEA app where we can enjoy and spred the love all over the world. Thanks for such a woundeful app and hatts off to all the hardworing staffs which has given us plesent suprise gift in pandemic situation of Covid-19. 
                Again, I am grateful for your time and effort that you have given us for awesome SWFTEA app.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>

           <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/henny_chang.jpg" class="testimonial-img" alt="">
              <h3>Henny_chang</h3>
              <h5>'China'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                我的身份证名是henny_chang
                我是Swftea应用程序的用户之一，在这个Swftea应用程序中，由于一些游戏，有趣的功能，我感到很舒服，很开心，并且员工和所有者对所有用户都非常友好和友善，在这个Swftea应用程序中，我发现了一些可以找到一些游戏功能的功能声音和胜利游戏也都有图片，对我来说，这是非常有趣的，目前还没有出现在任何应用程序中，
                谢谢swftea应用程序的存在，希望将来swftea对所有人都能进步和发展，永远成功。
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>

          <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/sangeh.jpg" class="testimonial-img" alt="">
              <h3>Sangeh</h3>
              <h5>'Indonesia'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Saya suka dengan app ini karena : Waktu saya mencoba app ini staff atau bos nya ramah dan baik, Sebelum update app ini bisa share foto di room chat. Jadi saya percaya dan optimis ke depannya app ini semakin bagus dan menyenangkan 
                Terutama game guessnya yang bikin penasaran, sistem merchan juga bagus tiap user busa melaporkan spending creditnya dan mereka akan mendapatkan cash back dari spending harian sebanyak 3%. Jadi seandainya kita kehabisan credit mereka bisa memberi kita bonus spending di hari berikutnya.

                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>


          <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/beauty.jpg" class="testimonial-img" alt="">
              <h3>Beauty.</h3>
              <h5>'Indonesia'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Saya menemukan sesuatu di Sweftea yang tidak ada pada tempat lain, yaitu sifat kekeluargaan.. keramahan yang diberikan didalamnya membuat saya betah berlama lama untuk tetap tinggal.. dan untuk tingkat bisnisnya menyediakan banyak peluang bagi mereka yang mempunyai keinginan untuk maju.. mereka para tim didalamnya selalu siap untuk membantu.. always happy 'n love sweftea
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>


          <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/venom1.png" class="testimonial-img" alt="">
              <h3>Venom1</h3>
              <h5>'Nepal'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Swftea app chalaunu ko euta beglai aaanada anubhav vayo...vannu parda mig chaalaune bela maa hami saayd teenage maa thim hola ra tetikher ko mazza teti khera ko tyo khusi feri lyaako xa swftea le......hami sabai jaana ailey aaf-aafno bebhar fasera baseko hunxam stressed depressed frustate in tnsd hunxam tara jaba swftea maa pasinxa ni khai tyo kura haru dimakh maa aaudai aaudaina...sathi vai sanga ko khusi haaso majak ..mann laagda game har thok paako xu swftea maa maile..saayad tei vara hola..swftea mero laagi euta ramro mann bhulane virtual bata real sathi baanune mero laagi euta thulo madhyam vayeko xa...yaanha maile staff dekhi lera user sabai frankly open minded vetaaye ra saayad ajha aaune din maa esari nai yo khusi haru xarxa swftea le vanne biswas greko xu ...ek sabda maa swftea ko experience vannu ho vane..its awesome.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>

          <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/joodak.jpg" class="testimonial-img" alt="">
              <h3>Joodak</h3>
              <h5>'Sudan'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                هذا التطبيق من اجمل التطبيقات التي استخدمتها ليس مثل غيره من ناحية افكار وتطور وتجديد بصورة يوميه وايضا يمكنك العمل فيه لكسب المال عن طرق كثيره وابسطها ان تكون تجار والكلام لا يكفي الا بالتجربة
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>

          <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/junkie.jpg" class="testimonial-img" alt="">
              <h3>Junkie</h3>
              <h5>'Qatar'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                GREAT  Team, GREAT App- Hail SwfTea!!!_ 
                Most of us had used Migme before at least once in its peak. After Migme App itself got inactive, I came across to so many Apps which was trying to give Migme vibes and trying to attract users. But after failing to get satisfied myself with those Apps I heard about this App and signed up for it. First experience was so much positive that staffs are so incredibly dedicated to improve App performance and positive towards user’s feedback. They constantly listened to users and tried to make it fun place rather than just an App. Got to know so many amazing peoples from across the countries, which is really amazing thing. Now I don’t feel it like Migme and neither want to, because SwfTea has different vibes which always attracts me; talking to nicest people, great App performance, great staffs, play incredibly fun games even though constant lose and so on.
                I would like to thank whole SwfTea Team and I am so glad that I found SwfTea and happy to share my experience. Hail SwfTea! Hail
                SwfTea-er!! Happy SwfTea-ing!
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>

          <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/singsz.png" class="testimonial-img" alt="">
              <h3>singsz</h3>
              <h5>'Nepal'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Before i used many social platforms /apps like swftea .
                But SWFTEA became addictive to me , because of its helpful staffs , friendly admins/ coaches  and lovely users . And also i made many friends through swftea. 
                And the main thing
                I found other apps had nepotism , but its totally not allowed in SWFTEA . Any one can be in any post with their capacity . 
                I will not force anyone to use this app , but alteast you have to try once . 
                Enjoy SWFTEAing.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>

          <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/dumunk.jpg" class="testimonial-img" alt="">
              <h3>Dumunk</h3>
              <h5>'Indonesia'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Swftea aplikasi yg sangat berbeda dengan aplikasi chatting lainnya . Swftea memberikan kenyamanan bagi para user . Stafnya pun sangat bersahabat . Ini yang membuat Saya bangga menggunakan Swftea . We love Swftea.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>

            <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/eastjava.jpg" class="testimonial-img" alt="">
              <h3>Eastjava</h3>
              <h5>'Indonesia'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
               Saya adalah pengguna pertama swftea sejak awal di luncurkan, selama saya join di swftea saya merasakan kesenangan, kenyamanan dan keramahan dari para pengguna,
               Di tambah lagi dengan fitur yang begitu menarik dari app swftea, yang belum pernah ada di app lainya, terima kasih swftea atas fitur menarik dan kejutan yang selalu membuat pengguna senang dan nyaman
               Salam sejahtera swftea.

                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>

          <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/l0v3rboy.png" class="testimonial-img" alt="">
              <h3>L0v3rboy</h3>
              <h5>'Nepal'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                SwfTea;one of the best online platform i have ever used.
                I have met many persons virtually in this app but i can sense true togetherness,brotherhood among varieties of personalities here.Not only the users from nepal but also from the various countries like Indonesia,Sudan,Brazil are sharing friendship here.We got chance to exchange the relegious norms and values and somehow got chance to learn a new language tho.
                Gaming experience in swftea is amazingly wow!
                Interaction of owner and devloper team is just wow!I loved the way they takes our feedbacks and how they supports us.
                Enjoy Swfting.
                Lots of love to all of my friends,brothers and sisters here.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>

          <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/max.png" class="testimonial-img" alt="">
              <h3>Max</h3>
              <h5>'Malta'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                First of all, I m thankful to swftea management team for giving me opportunity to serve as an legend post. I  have an experience of almost 1 and half months in this app.I feel very honored n grateful to be able to help users and give my valuable time n effort to this app. The staffs here are very helpful, supportive and friendly as well. Legend ID color is very attractive n well suited, also it's unique n distinguished unlike other apps. Once again , I m very thankful for swftea management team for listening to and applying some of my ideas and concepts as a legend post. In the coming near future , if I get a chance of being an Admin/staff post, I can give my best effort in helping users...also I m capable of providing with more helpful, valuable ideas and concepts for the better  development of this app making the app worth user-friendly, trust worthy and reliable.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>

          <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/blueeye.jpg" class="testimonial-img" alt="">
              <h3>Blueeye</h3>
              <h5>'Hong Kong'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                saya user dari indonesia, kalian Mau chatting seru sambil main game? Kalau begitu Anda bisa coba app chating SWFTEA Aplikasi ini bisa dibilang aplikasi serba guna. Kenapa? Sebab, di SWFTEA anda bisa bertemu dengan orang baru,bukan user indonesia saja tapi banyak juga user luar negeri, main game sambil chatting,dan banyak lagi kejutan heboh dari owner dan staff nya,di tambah lagi fiturnya keren dan terang, tunggu apalagi ayo buruan download app SWFTEA, aq tunggu ya,,, my friends
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>

          <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/spider.png" class="testimonial-img" alt="">
              <h3>Spider</h3>
              <h5>'Nepal'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                नमस्कार सबैमा  हामीहरु सुरु सुरुको समयमा migme app चलाउने  अनि त्यससँगै हराउने नयाँ  नयाँ  साथिहरु बनाउने गेमहरु खेल्दै उपहार थप्न विभिन्न रूमहरुमा जाने गरिन्थो। तर बिस्तारै migme बन्द भएको कारणले गर्दा अन्य त्य्स्तै app हरु प्रयोग गर्ने सुरु गरे तर कुनै पनि app को प्रयोग आफ्नु चित्तबुझ्दो नरहेको । admin र management प्रयोगकर्त लाई ध्यान न दिने अनि अन्य विभिन्न कारणहरु ले गर्दा कुनै पनि app प्रयोग न गरि बसेको समयमा यो app को बारेमा थाहा भयो र चलाउन थाले। सुरु सुरु मा केही समस्या भएको भएता पनि management र developer को आफ्नु अथक प्रयास प्रयोगकर्ताको को सल्लाह सबै लिएर अहिले यो अवस्था सम्म आई पुगेको छ । यो app चलाउन सुरु गरे पछी पुराना साथिहरु सङ्ग पुन भेटने अन्य नयाँ साथिहरु बनाउने मौका पनि पाएको कारणले गर्दा धेरै खुसी लगेको छ ।
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>

           <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/komando.jpg" class="testimonial-img" alt="">
              <h3>Komando</h3>
              <h5>'Indonesia'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Saya adalah salah satu users di Swftea, dengan id komando. Selama saya chat dengan menggunakan app Swftea saya cukup menikmatinya. Apalagi dengan Guess Game yang menarik dan asik. Cukup membuat saya betah. Game dan chat yang menarik buat saya. Bagi yang ingin bergabung ke Swftea sila download app nya. Enjoy chat dan happy game. Semoga kedepan Swftea semakin berkembang dan maju, bisa di nikmati users dari kalangan MILENIAL. Sekian uwik2 dari saya.Salam Swfting.

                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>

          <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/diamond.jpg" class="testimonial-img" alt="">
              <h3>Diamond</h3>
              <h5>'Indonesia'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                Halo guys ada app seru neh, nama nya SWFTEA,pengguna nya sudah cukup banyak lho, salah 1 nya aq aq user indonesia dan ID ku ( diamond ).
                App ini bukan saja untuk android, tapi support untuk IOS juga lho. selain bisa chat juga, kita bisa bermain bermacam2 jenis game lho, eits yg lebih seru lagi kita bisa ber interaksi dengan user2 dari berbagai negara lho so di jamin bertambah deh teman2 mu.ayo buruan download app nya di playstore dan ios, aq tunggu ya

                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>

          <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/inocentpunk.jpg" class="testimonial-img" alt="">
              <h3>Inocentpunk</h3>
              <h5>'Nepal'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                So Far my experience in swftea is awesome. Due to Swftea I  have made a connection with different Nations  people so that now  feel i am one of them. Thanks for the team of swftea which made all possible and made an app that helps people to connect with each.
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>

          <div class="testimonial-wrap">
            <div class="testimonial-item">
              <img src="assets/img/testimonials/punkonuz.jpg" class="testimonial-img" alt="">
              <h3>Punkonuz</h3>
              <h5>'UAE'</h5>
              <p>
                <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                SWFTEA app is great and the site is awesome for all people. I have a lot of app with a similar purpose but this is by far the best. Easy to use and convincent for everyone. Interface is also great. love it. I spend too much time on this one. I am feeling very easy to do social chat/grp chat without any delay. It has a All Staff/mod/mentor/Users are really friendly and simple interface.It help a lot of work. A good thing we have internations friends without any Discrimination too close here. I am looking more if any available in future then it would be great,simply. This app is very useful !!
                <i class="bx bxs-quote-alt-right quote-icon-right"></i>
              </p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Testimonials Section -->

    <!-- ======= Pricing Section ======= -->
    <section id="pricing" class="pricing">
      <div class="container">

        <div class="section-title">
          <h2>Pricing</h2>
          <p>This is our Mentor/Merchant pricing details which users can get on the App. Users can contact any official staffs for further queries about pricing or contact us via email.</p>
        </div>

        <div class="row no-gutters">

          <div class="col-lg-4 box" data-aos="fade-right">
            <h3 style="color:#ff00ff;">Headmentor</h3>
            <h4>$500</h4>
            <ul>
              <li><i class="bx bx-check"></i> Will get credits plus Pink color ID and access to create mentors</li>
              <li><i class="bx bx-check"></i> Wil get 3% trial revenue from spent of their tagged mentors and 15% bonus trial from the total trial earned by their tagged mentors</li>
              <li><i class="bx bx-check"></i> Can be known from their pink color ID, badges in profle and among people list inside swftea world from App menu</li>
              <!-- <li class="na"><i class="bx bx-x"></i> <span>Pharetra massa massa ultricies</span></li>
              <li class="na"><i class="bx bx-x"></i> <span>Massa ultricies mi quis hendrerit</span></li> -->
            </ul>
            <a href="#" class="get-started-btn">Get Started</a>
          </div>

          <div class="col-lg-4 box featured" data-aos="fade-up">
            <h3 style="color:#ff0000;">Mentor</h3>
            <h4>$200</h4>
            <ul>
              <li><i class="bx bx-check"></i> Will get credits plus Red color ID and access to create merchants</li>
              <li><i class="bx bx-check"></i> Wil get 2.75% trial revenue from spent of their tagged merchants and 10% bonus trial from the total trial earned by their tagged merchants</li>
              <li><i class="bx bx-check"></i> Can be known from their Red color ID, badges in profle and among people list inside swftea world from App menu</li>
            </ul>
            <a href="#" class="get-started-btn">Get Started</a>
          </div>

          <div class="col-lg-4 box" data-aos="fade-left">
            <h3 style="color:#630094;">Merchant</h3>
            <h4>$45</h4>
            <ul>
              <li><i class="bx bx-check"></i> Will get credits plus Purple color ID and can sell to normal users</li>
              <li><i class="bx bx-check"></i> Merchants are official sellers for normal users so they will earn 2.5% trial revenue from the spent of their tagged users</li>
              <li><i class="bx bx-check"></i> Can be known from their Purple color ID, badges in profle and among people list inside swftea world from App menu</li>
            </ul>
            <a href="#" class="get-started-btn">Get Started</a>
          </div>

        </div>

      </div>
    </section><!-- End Pricing Section -->

    <!-- ======= Frequently Asked Questions Section ======= -->
    <!-- <section id="faq" class="faq section-bg">
      <div class="container">

        <div class="section-title">

          <h2>Frequently Asked Questions</h2>
          <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
        </div>

        <div class="accordion-list">
          <ul>
            <li data-aos="fade-up">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" class="collapse" href="#accordion-list-1">Non consectetur a erat nam at lectus urna duis? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="accordion-list-1" class="collapse show" data-parent=".accordion-list">
                <p>
                  Feugiat pretium nibh ipsum consequat. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="100">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#accordion-list-2" class="collapsed">Feugiat scelerisque varius morbi enim nunc? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="accordion-list-2" class="collapse" data-parent=".accordion-list">
                <p>
                  Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="200">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#accordion-list-3" class="collapsed">Dolor sit amet consectetur adipiscing elit? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="accordion-list-3" class="collapse" data-parent=".accordion-list">
                <p>
                  Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="300">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#accordion-list-4" class="collapsed">Tempus quam pellentesque nec nam aliquam sem et tortor consequat? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="accordion-list-4" class="collapse" data-parent=".accordion-list">
                <p>
                  Molestie a iaculis at erat pellentesque adipiscing commodo. Dignissim suspendisse in est ante in. Nunc vel risus commodo viverra maecenas accumsan. Sit amet nisl suscipit adipiscing bibendum est. Purus gravida quis blandit turpis cursus in.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="400">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#accordion-list-5" class="collapsed">Tortor vitae purus faucibus ornare. Varius vel pharetra vel turpis nunc eget lorem dolor? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="accordion-list-5" class="collapse" data-parent=".accordion-list">
                <p>
                  Laoreet sit amet cursus sit amet dictum sit amet justo. Mauris vitae ultricies leo integer malesuada nunc vel. Tincidunt eget nullam non nisi est sit amet. Turpis nunc eget lorem dolor sed. Ut venenatis tellus in metus vulputate eu scelerisque.
                </p>
              </div>
            </li>

          </ul>
        </div>

      </div>
    </section> -->
    <!-- End Frequently Asked Questions Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container">

        <div class="section-title">
          <h2>Contact</h2>
          <p>If you have any queries feel free to contact us to the details provided below and we will answer you shortly. Any feedbacks and suggestions are highly appreciated. 
          </p>
        </div>

        <div class="row">

          <div class="col-lg-6">
            <div class="row">
              <div class="col-lg-6 info" data-aos="fade-up">
                <i class="bx bx-map"></i>
                <h4>Address</h4>
                <p>Kathmandu,<br>Bagmati Province, 44600</p>
              </div>
              <div class="col-lg-6 info" data-aos="fade-up" data-aos-delay="100">
                <i class="bx bx-phone"></i>
                <h4>Call Us</h4>
                <p>+9779863725198<br>+9779807998889 </p>
              </div>
              <div class="col-lg-6 info" data-aos="fade-up" data-aos-delay="200">
                <i class="bx bx-envelope"></i>
                <h4>Email Us</h4>
                <p>swftea@swftea.com<br>contact@swftea.com<br>support@swftea.com<br>swfteaofficial@gmail.com</p>
              </div>
              <div class="col-lg-6 info" data-aos="fade-up" data-aos-delay="300">
                <i class="bx bx-time-five"></i>
                <h4>Working Hours</h4>
                <p>Sun - Fri: 10AM to 10PM<br>Saturday: 10AM to 1PM</p>
              </div>
            </div>
          </div>

          <!-- <div class="col-lg-6">
            <form action="forms/contact.php" method="post" role="form" class="php-email-form" data-aos="fade-up">
              <div class="form-group">
                <input placeholder="Your Name" type="text" name="name" class="form-control" id="name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <input placeholder="Your Email" type="email" class="form-control" name="email" id="email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <input placeholder="Subject" type="text" class="form-control" name="subject" id="subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <textarea placeholder="Message" class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us"></textarea>
                <div class="validate"></div>
              </div>
              <div class="mb-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div>
              <div class="text-center"><button type="submit">Send Message</button></div>
            </form>
          </div> -->

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact" data-aos="fade-up">
            <h3>SWFTEA</h3>
            <p>
              Kathmandu <br>
              Bagmati Province, 44600<br>
              Nepal <br><br>
              <strong>Phone:</strong> +9779863725198<br>
              <strong>Email:</strong> swfteaofficial@gmail.com<br>
            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links" data-aos="fade-up" data-aos-delay="100">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#details">About us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#gallery">Gallery</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#pricing">Pricing</a></li>
              <!-- <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li> -->
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links" data-aos="fade-up" data-aos-delay="200">
            <h4>Our Features</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#features">Chats</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#features">Gifts</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#features">Games</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#features">Mentor/Merchant Programme</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#Features">Custom Store</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links" data-aos="fade-up" data-aos-delay="300">
            <h4>Our Social Networks</h4>
            <p>You can reach us via our social networks too.</p>
            <div class="social-links mt-3">
              <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="https://www.facebook.com/groups/757010658417938" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
             <!--  <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a> -->
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container py-4">
      <!-- <div class="copyright">
        &copy; Copyright <strong><span>Appland</span></strong>. All Rights Reserved
      </div> -->
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/free-bootstrap-app-landing-page-template/ -->
        <!-- Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a> -->
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>
